package org.tinygroup.webservicetest;

import java.io.Serializable;

public interface User extends Serializable {
	public String getName();
	public String getAge();
}
